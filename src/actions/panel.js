import axios from 'axios';
import { panelTypes } from '../actionTypes';

export const approve = payload => ({
  type: panelTypes.APPROVED,
  payload,
});

export const doesNotApply = payload => ({
  type: panelTypes.NOT_APPLY,
  payload,
});

export const sendSuccess = payload => ({
  type: panelTypes.SEND_SUCCESS,
  payload,
});

export const sendFail = payload => ({
  type: panelTypes.FAIL_SUCCESS,
  payload,
});

export const passVerify = () => (dispatch, getState) => getState().panel.pass;
export const successSend = () => (dispatch, getState) => getState().panel.send;

export const verifyZipCode = zipcode => () => axios.post('verifyZip', { zipcode })
  .then(response => response.data.pass)
  .catch(() => false);

export const sendInfo = info => dispatch => axios.post('/mail', info)
  .then((response) => {
    dispatch(sendSuccess());
    if (response.data.response === 'success') { return true; }
    return false;
  })
  .catch(() => {
    dispatch(sendFail());
    return false;
  });
