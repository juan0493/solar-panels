import React from 'react';
import { Translate } from 'react-localize-redux';

import { Footer } from '../Footer/Footer';
import { Container } from '../Common/Container';
import { Header } from '../Header/Header';

import '../../style/About.styl';
import fbIcon from '../../img/icons/ic_facebook.png';
import panels from '../../img/picture.png';

export const About = () => (
  <section id="main">
    <Header />
    <Container id="about">
      <div className="row">
        <div className="col s12">
          <h1><Translate id="About.title" /></h1>
        </div>
      </div>
      <div className="row valign-wrapper">
        <div className="col s12 m6 l6 xl7">
          <p><Translate id="About.text1" /></p>
          <p><Translate id="About.text2" /></p>
          <p><Translate id="About.text3" /></p>
        </div>
        <div className="col s12 m6 l6 xl5 media">
          <img src={panels} alt="Solar panels" className="responsive-img" />
          <p>
            <img src={fbIcon} alt="Solar panels" />
            #GOING<span>SOLAR</span>
          </p>
        </div>
      </div>
      <div className="row">
        <div className="col s12">
          <p className="justify-align"><Translate id="About.text4" /></p>
          <p className="justify-align"><Translate id="About.text5" /></p>
        </div>
      </div>
    </Container>
    <Footer />
  </section>
);
