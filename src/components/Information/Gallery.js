import React from 'react';

import { Footer } from '../Footer/Footer';
import { Container } from '../Common/Container';
import { Header } from '../Header/Header';

import '../../style/Gallery.styl';

export const Gallery = () => (
  <section id="main">
    <Header />
    <Container id="gallery">
      <div className="photo photo1" />
      <div className="photo photo2" />
      <div className="photo photo3" />
      <div className="photo photo4" />
      <div className="photo photo5" />
      <div className="photo photo6" />
      <div className="photo photo7" />
      <div className="photo photo8" />
    </Container>
    <Footer />
  </section>
);
