import React from 'react';
import { Translate } from 'react-localize-redux';

import { Footer } from '../Footer/Footer';
import { Container } from '../Common/Container';
import { Header } from '../Header/Header';

import '../../style/About.styl';
import fbIcon from '../../img/icons/ic_facebook.png';
import panel from '../../img/comercial.png';

export const Commercial = () => (
  <section id="main">
    <Header />
    <Container id="commercial">
      <div className="row">
        <div className="col s12">
          <h1><Translate id="Commercial.title" /></h1>
        </div>
      </div>
      <div className="row valign-wrapper">
        <div className="col s12 m6 l6 xl7">
          <p><Translate id="Commercial.text1" /></p>
          <p><Translate id="Commercial.text2" /></p>
          <div className="list">
            <p><Translate id="Commercial.list.title" /></p>
            <ul>
              <li><Translate id="Commercial.list.item1" /></li>
              <li><Translate id="Commercial.list.item2" /></li>
              <li><Translate id="Commercial.list.item3" /></li>
              <li><Translate id="Commercial.list.item4" /></li>
              <li><Translate id="Commercial.list.item5" /></li>
              <li><Translate id="Commercial.list.item6" /></li>
            </ul>
          </div>
        </div>
        <div className="col s12 m6 l6 xl5 media">
          <img src={panel} alt="Solar panels" className="responsive-img" />
          <p>
            <img src={fbIcon} alt="Solar panels" />
            #GOING<span>SOLAR</span>
          </p>
        </div>
      </div>
    </Container>
    <Footer />
  </section>
);
