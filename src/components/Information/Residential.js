import React from 'react';
import { Translate } from 'react-localize-redux';

import { Footer } from '../Footer/Footer';
import { Container } from '../Common/Container';
import { Header } from '../Header/Header';

import '../../style/About.styl';
import fbIcon from '../../img/icons/ic_facebook.png';
import family from '../../img/family.png';

export const Residential = () => (
  <section id="main">
    <Header />
    <Container id="residential">
      <div className="row">
        <div className="col s12">
          <h1><Translate id="Residential.title" /></h1>
        </div>
      </div>
      <div className="row valign-wrapper">
        <div className="col s12 m6 l6 xl7">
          <p><Translate id="Residential.text1" /></p>
          <p><Translate id="Residential.text2" /></p>
          <p><Translate id="Residential.text3" /></p>
        </div>
        <div className="col s12 m6 l6 xl5 media">
          <img src={family} alt="Family" className="responsive-img" />
          <p>
            <img src={fbIcon} alt="Solar panels" />
            #GOING<span>SOLAR</span>
          </p>
        </div>
      </div>
    </Container>
    <Footer />
  </section>
);
