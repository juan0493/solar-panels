import React from 'react';
import PropTypes from 'prop-types';

import '../../style/Cont.styl';

export const Container = ({ id, children }) => (
  <section className="bgContainer" id={id}>
    <div className="container">
      {children}
    </div>
  </section>
);

Container.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  id: PropTypes.string.isRequired,
};
