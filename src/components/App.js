import axios from 'axios';
import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom';
import Analytics from 'react-router-ga';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import { initialize, addTranslation } from 'react-localize-redux';

import '../style/App.styl';
import { rootReducer } from '../reducers';
import { host, gaConfig } from './config';
import Home from './Home';
import { About, Residential, Gallery, Commercial } from './Information';
import { Error404 } from './Error404';
import lang from '../translations/en.json';

axios.defaults.baseURL = host.urlApi;
const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));

// Initialize supported languages
const languages = ['es', 'en'];
store.dispatch(initialize(languages));
store.dispatch(addTranslation(lang));

const App = () => (
  <Provider store={store}>
    <Router>
      <Analytics id={gaConfig.ID}>
        <Switch>
          <Route
            exact
            path="/"
            component={Home}
          />
          <Route
            exact
            path="/about"
            component={About}
          />
          <Route
            exact
            path="/gallery"
            component={Gallery}
          />
          <Route
            exact
            path="/residential"
            component={Residential}
          />
          <Route
            exact
            path="/commercial"
            component={Commercial}
          />
          <Route component={Error404} />
        </Switch>
      </Analytics>
    </Router>
  </Provider>
);

export default App;
