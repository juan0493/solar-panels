import React from 'react';
import moment from 'moment';

import '../../style/Footer.styl';
import C1 from '../../img/icons/c1.png';
import C2 from '../../img/icons/c2.png';
import C3 from '../../img/icons/c3.png';
import C4 from '../../img/icons/c4.png';

export const Footer = () => (
  <footer>
    <div className="container">
      <div className="row">
        <div className="left-align col s12 m4 l4 xl4">
          <h2>Certifications: </h2>
        </div>
        <div className="center-align col s12 m8 l4 xl4">
          <a href="#!"><img className="small" src={C1} alt="CEC" /></a>
          <a href="https://www.iso.org/iso-9001-quality-management.html" rel="noopener noreferrer" target="_blank"><img className="big" src={C2} alt="ISO 9001" /></a>
          <p>
            <span>IEC61215</span>
            <span>IEC61730</span>
            <span>UL1703</span>
            <span>Conformity to CE</span>
          </p>
          <a href="https://www.tuv.com/spain/es/" rel="noopener noreferrer" target="_blank"><img className="big" src={C3} alt="tuv rheinland" /></a>
          <a href="http://www.fsec.ucf.edu" rel="noopener noreferrer" target="_blank"><img className="small" src={C4} alt="FSEC" /></a>
        </div>
        <div className="right-align col s12 m12 l4 xl4">
          <p>Florida Solar Power Copyright © {moment().year()}</p>
        </div>
      </div>
    </div>
  </footer>
);
