import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Footer } from './Footer/Footer';
import { Header } from './Header/Header';
import { PoweredBy } from './PoweredBy/PoweredBy';
import Form from './Form/Form';
import '../style/App.styl';

class Home extends Component {
  state = {}

  render() {
    return (
      <section id="main">
        <Header />
        <Form />
        <PoweredBy />
        <Footer />
      </section>
    );
  }
}

export default connect(null, null)(Home);
