import React from 'react';
import { Link } from 'react-router-dom';
import { Translate } from 'react-localize-redux';


import '../../style/Header.styl';
import Logo from '../../img/logo.png';

/* eslint jsx-a11y/anchor-is-valid: off */
export const Header = () => (
  <header>
    <div className="container">
      <div className="row">
        <div className="col s12 m6 l6 xl6"><Link to="/"><img src={Logo} alt="Florida Solar Power" /></Link></div>
        <div className="col s12 m6 l6 xl6">
          <nav>
            <Link to="/"><Translate id="Header.apply" /></Link>
            <Link to="/about"><Translate id="Header.about" /></Link>
            <Link to="/residential"><Translate id="Header.residential" /></Link>
            <Link to="/commercial"><Translate id="Header.commercial" /></Link>
            <Link to="/gallery"><Translate id="Header.gallery" /></Link>
          </nav>
        </div>
      </div>
    </div>
  </header>
);
