import React from 'react';

const divStyle = {
  textAlign: 'center',
};

export const Error404 = () => (
  <div>
    <h1 style={divStyle}>
      404 - Page not found
    </h1>
  </div>
);

