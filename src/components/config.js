module.exports = {
  host: {
    urlApi: 'https://floridasp.com/api/',
  },
  gaConfig: {
    ID: 'UA-89112654-2',
  },
};

// urlApi: 'http://localhost:5000/api/',
// urlApi: 'https://solar-panels.herokuapp.com/api/',
// urlApi: 'https://floridasp.com/api/',
// urlApi: 'https://test-solar-panels.herokuapp.com/api/',

