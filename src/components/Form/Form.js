import React, { Component } from 'react';
import { connect } from 'react-redux';
import ReactGA from 'react-ga';
import PropTypes from 'prop-types';
import swal from 'sweetalert';
import _ from 'lodash';
import { Translate } from 'react-localize-redux';

import { panelActions } from '../../actions';
import { Information } from './components/Information';
import { gaConfig } from '../config';

import '../../style/Form.styl';

class Form extends Component {
  static propTypes = {
    dispatchApprove: PropTypes.func.isRequired,
    dispatchNotApply: PropTypes.func.isRequired,
    dispatchPassVerify: PropTypes.func.isRequired,
    dispatchSendInfo: PropTypes.func.isRequired,
    dispatchSuccessSend: PropTypes.func.isRequired,
    dispatchVerifyZipCode: PropTypes.func.isRequired,
  };

  state = {
    applyMsg: false,
    buttonText: '¡PÁSATE A SOLAR!',
    error: false,
    step1: true,
    step2: false,
    inputs: {
      name: '',
      mail: '',
      phone: '',
      time: 'morning',
      zipcode: '',
    },
    processing: false,
  };

  // Refs
  formHeadTitle = React.createRef();
  step1Visible = React.createRef();
  step2Visible = React.createRef();

  verify = (ev) => {
    ev.preventDefault();
    if (!this.props.dispatchPassVerify()) {
      this.setState({ processing: true });

      setTimeout(() => {
        this.props.dispatchVerifyZipCode(this.state.inputs.zipcode)
          .then((response) => {
            if (response) {
              this.setState({
                applyMsg: true,
                buttonText: 'Solo un paso más',
                error: false,
              });

              setTimeout(() => {
                this.props.dispatchApprove();
                this.setState({ applyMsg: false });
                this.formHeadTitle.current.classList.add('steps-action');
              }, 2500);
            } else {
              this.setState({ error: true });
              this.props.dispatchNotApply();
            }
          }).then(() => {
            this.setState({ processing: false });
          });
      }, 2000);
    } else if (this.state.step1) {
      this.setState({
        step1: false,
        step2: true,
        buttonText: 'Enviar información',
      });
      /* eslint no-unused-vars: off */
      /* eslint no-undef: off */
      const elem = document.querySelector('select');
      M.FormSelect.init(elem);
    } else if (this.state.inputs.phone !== '') {
      this.setState({ processing: true });
      this.props.dispatchSendInfo({
        name: this.state.inputs.name,
        mail: this.state.inputs.mail,
        phone: this.state.inputs.phone,
        zipcode: this.state.inputs.zipcode,
        time: this.state.inputs.time,
      })
        .then((response) => {
          if (response) {
            this.analyticsEvent();
            swal('¡¡Hemos terminado!!', 'Contactaremos contigo tan pronto como sea posible', 'success');
            this.formHeadTitle.current.classList.remove('steps-action');
          } else {
            swal('Error', 'Se produjo un error mientras se enviaba tu mail, intenta nuevamente', 'error');
          }
        }).then(() => {
          this.setState({ processing: false });
        });
    }
  };

  analyticsEvent = () => {
    ReactGA.initialize(gaConfig.ID);
    ReactGA.event({
      category: 'Contact',
      action: 'User sent contact information',
    });
  };

  alert = () => {
    swal('¡Lo sentimos mucho!', 'Desafortunadamente tu zipcode no aplica para una visita técnica aún.', 'error');
    // Reset error state
    this.setState({ error: false });
  }

  changeValue = (e) => {
    e.persist();
    this.setState(prevState => ({
      inputs: _.set(
        prevState.inputs,
        `${e.target.name}`,
        e.target.value,
      ),
    }));
  }

  render() {
    return (
      <section id="form">
        {this.state.error && this.alert()}
        <div className="container">
          <div className="row valign-wrapper">
            <div className="info col s12 m6 l6 xl6">
              <Information />
            </div>
            <div className="col s12 m6 l6 xl6">
              <form method="post" action="." className="form" onSubmit={this.verify} ref={this.formHeadTitle}>
                <div className="text">
                  <h2><span>¡Deje de perder</span><span>su dinero!</span></h2>
                  <div className="shape left" />
                  <div className="shape right" />
                  {this.state.processing &&
                    <div className="progress">
                      <div className="indeterminate" />
                    </div>
                  }
                </div>

                {!this.props.dispatchSuccessSend() &&
                  <div className="call-to-action">
                    {this.state.applyMsg &&
                      <div className="row msg">
                        <div className="col s12 m12 l4 xl4">
                          <div className="swal-icon swal-icon--success">
                            <span className="swal-icon--success__line swal-icon--success__line--long" />
                            <span className="swal-icon--success__line swal-icon--success__line--tip" />
                            <div className="swal-icon--success__ring" />
                            <div className="swal-icon--success__hide-corners" />
                          </div>
                        </div>
                        <h3 className="col s12 m12 l8 xl8"><Translate id="Form.Action.codePass" /></h3>
                      </div>
                    }

                    {(!this.props.dispatchPassVerify() && !this.state.applyMsg) &&
                      <input required type="text" name="zipcode" id="zipcode" placeholder="Zip code" className="validate" onChange={this.changeValue} />
                    }

                    {this.props.dispatchPassVerify() &&
                      <div className="steps">


                        <div className={`step1 ${this.state.step1 ? 'current' : ''}`}>
                          <div className="row">
                            <div className="input-field col s12">
                              <input required placeholder="Nombre" id="name" name="name" type="text" className="validate" onChange={this.changeValue} />
                            </div>
                          </div>
                          <div className="row">
                            <div className="input-field col s12">
                              <input required placeholder="Dirección" id="address" name="address" type="text" className="validate" onChange={this.changeValue} />
                            </div>
                          </div>
                          <div className="row">
                            <div className="input-field col s12">
                              <input required placeholder="Email" id="mail" name="mail" type="email" className="validate" onChange={this.changeValue} />
                            </div>
                          </div>
                        </div>

                        <div className={`step2 ${this.state.step2 ? 'current' : ''}`}>
                          <div className="row">
                            <div className="input-field col s12">
                              <input placeholder="Teléfono" id="phone" name="phone" type="text" className="validate" onChange={this.changeValue} />
                            </div>
                          </div>
                          <div className="row">
                            <div className="input-field col s12">
                              <select
                                required
                                id="time"
                                name="time"
                                value="morning"
                                onChange={this.changeValue}
                              >
                                <option value="morning">Mañana</option>
                                <option value="afternoon">Tarde</option>
                                <option value="night">Noche</option>
                              </select>
                              <label htmlFor="time">De preferencia llamar en la?</label>
                            </div>
                          </div>
                        </div>
                      </div>
                    }

                    {
                      this.state.processing ?
                        <button disabled>Procesando...</button>
                        : <button>{this.state.buttonText}</button>
                    }
                    <p className="intro"><Translate id="Form.Action.smallText" /></p>
                  </div>
                }

                {this.props.dispatchSuccessSend() &&
                  <div className="complete">
                    <h3>¡Hemos terminado!</h3>
                    <p>¡Espera por nuestra llamada muy pronto!</p>
                  </div>
                }
              </form>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

const mapDispatchToProps = {
  dispatchApprove: panelActions.approve,
  dispatchNotApply: panelActions.doesNotApply,
  dispatchPassVerify: panelActions.passVerify,
  dispatchSendInfo: panelActions.sendInfo,
  dispatchSuccessSend: panelActions.successSend,
  dispatchVerifyZipCode: panelActions.verifyZipCode,
};

export default connect(null, mapDispatchToProps)(Form);
