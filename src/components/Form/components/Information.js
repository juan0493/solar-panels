import React from 'react';
import { Translate } from 'react-localize-redux';

import eco from '../../../img/eco.png';
import embasy from '../../../img/embasy.png';
import saving from '../../../img/saving.png';

export const Information = () => (
  <div>
    <h1><Translate id="Form.info.title" /></h1>
    <table>
      <tbody>
        <tr>
          <td> <div className="Icon"> <img src={saving} alt="Icon Saving" /> </div> </td>
          <td>
            <p>
              <Translate id="Form.info.subtitle1" />
              <Translate id="Form.info.text1" />
            </p>
          </td>
        </tr>
        <tr>
          <td> <div className="Icon"> <img src={embasy} alt="Icon Embasy" /> </div> </td>
          <td>
            <p>
              <Translate id="Form.info.subtitle2" />
              <Translate id="Form.info.text2" />
            </p>
          </td>
        </tr>
        <tr>
          <td> <div className="Icon"> <img src={eco} alt="Icon Eco" /> </div> </td>
          <td>
            <p>
              <Translate id="Form.info.subtitle3" />
              <Translate id="Form.info.text3" />
            </p>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
);
