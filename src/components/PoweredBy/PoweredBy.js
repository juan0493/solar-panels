import React from 'react';

import '../../style/PoweredBy.styl';
import Logo1 from '../../img/logos/logo1.png';
import Logo2 from '../../img/logos/logo2.png';
import Logo3 from '../../img/logos/logo3.png';
import Logo4 from '../../img/logos/logo4.png';
import Logo5 from '../../img/logos/logo5.png';
import Logo6 from '../../img/logos/logo6.png';
import Logo7 from '../../img/logos/logo7.png';

export const PoweredBy = () => (
  <section id="poweredBy">
    <div className="container">
      <h2>Powered by:</h2>
      <div>
        <a href="#!"><img className="big" src={Logo1} alt="Alliance NRG program" /></a>
        <a href="#!"><img className="big" src={Logo2} alt="Renew Financial" /></a>
        <a href="#!"><img className="small" src={Logo3} alt="FPL" /></a>
        <a href="#!"><img className="big" src={Logo4} alt="Service Finance Company, LLC" /></a>
      </div>
      <div>
        <a href="#!"><img className="med" src={Logo5} alt="SolarTech Universal" /></a>
        <a href="#!"><img className="med" src={Logo6} alt="Enphase" /></a>
        <a href="#!"><img className="epiq" src={Logo7} alt="EPIQ" /></a>
      </div>
    </div>
  </section>
);
