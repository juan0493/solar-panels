import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import swal from 'sweetalert';
import _ from 'lodash';
import { Animated } from 'react-animated-css';
import ReactGA from 'react-ga';

import { H2 } from './H2';
import { Button } from './Button';
import { panelActions } from '../../../actions';

let instance = null;

class Form extends Component {
  static propTypes = {
    dispatchVerifyZipCode: PropTypes.func.isRequired,
    dispatchSendInfo: PropTypes.func.isRequired,
    dispatchApprove: PropTypes.func.isRequired,
    dispatchNotApply: PropTypes.func.isRequired,
    passVerify: PropTypes.bool.isRequired,
    successSend: PropTypes.bool.isRequired,
  }

  state = {
    apply: false,
    buttonText: 'Apply for free visit',
    error: false,
    step1: true,
    step2: false,
    inputs: {
      name: '',
      mail: '',
      phone: '',
      time: 'morning',
      zipcode: '',
    },
    processing: false,
  };

  analyticsEvent = () => {
    ReactGA.initialize('UA-89112654-2');
    ReactGA.event({
      category: 'Contact',
      action: 'User sent contact information',
    });
  };

  verify = (ev) => {
    ev.preventDefault();
    if (!this.props.passVerify) {
      this.setState({
        processing: true,
      });

      setTimeout(() => {
        this.props.dispatchVerifyZipCode(this.state.inputs.zipcode)
          .then((response) => {
            if (response) {
              this.setState({
                buttonText: 'Send contact information',
                error: false,
              });
              this.setState({
                apply: true,
              });
              setTimeout(() => {
                this.props.dispatchApprove();
                this.setState({
                  apply: false,
                });
              }, 2000);
            } else {
              this.setState({
                error: true,
              });
              this.props.dispatchNotApply();
            }
          }).then(() => {
            this.setState({
              processing: false,
            });
          });
      }, 2000);
    } else if (this.state.step1) {
      this.setState({
        step1: false,
        step2: true,
      });
      const elem = document.querySelector('select');
      /* eslint no-unused-vars: off */
      /* eslint no-undef: off */
      instance = M.FormSelect.init(elem);
    } else if (this.state.inputs.phone !== '') {
      this.setState({
        processing: true,
      });
      this.props.dispatchSendInfo({
        name: this.state.inputs.name,
        mail: this.state.inputs.mail,
        phone: this.state.inputs.phone,
        zipcode: this.state.inputs.zipcode,
        time: this.state.inputs.time,
      })
        .then((response) => {
          if (response) {
            this.analyticsEvent();
            swal({
              title: 'We are done!!',
              text: 'We will contact you as soon as possible',
              icon: 'success',
            });
          } else {
            swal({
              title: 'Error',
              text: 'There was an error sending your mail, try again',
              icon: 'error',
            });
          }
        }).then(() => {
          this.setState({
            processing: false,
          });
        });
    }
  }

  alert = () => {
    swal({
      title: 'We are very sorry!',
      text: 'Unfortunately, the zip code provided does not apply for a technical visit yet',
      icon: 'error',
    });
    // Reset error state
    this.setState({
      error: false,
    });
  }

  changeValue = (e) => {
    e.persist();
    this.setState(prevState => ({
      inputs: _.set(
        prevState.inputs,
        `${e.target.name}`,
        e.target.value,
      ),
    }));
  }

  render() {
    return (
      <div className="containerForm">
        <div className="headerForm">
          <H2 customClass="shadow" text="HOW MUCH MONEY ARE YOU WASTING ON YOUR CURRENT BILL?" />
        </div>
        <div className="bodyForm">
          { this.state.processing &&
            <div className="progress">
              <div className="indeterminate" />
            </div>
          }

          { this.state.apply &&
            <div>
              <div className="swal-icon swal-icon--success">
                <span className="swal-icon--success__line swal-icon--success__line--long" />
                <span className="swal-icon--success__line swal-icon--success__line--tip" />
                <div className="swal-icon--success__ring" />
                <div className="swal-icon--success__hide-corners" />
              </div>
              <h3>Congratulations, your zip code applies for a technical visit</h3>
            </div>
          }

          { (!this.props.successSend && !this.state.apply) &&
            <form action="." method="post" onSubmit={this.verify}>
              { this.state.error && this.alert() }
              { !this.props.passVerify &&
                <div>
                  <p className="intro">
                    Apply to receive a totally free visit
                    from our technician for an inspection
                  </p>
                  <input required type="text" name="zipcode" id="zipcode" placeholder="Zip code" className="validate" onChange={this.changeValue} />
                </div>
              }

              { this.props.passVerify &&
                <div className="steps">

                  <Animated animationIn="bounceInDown" animationOut="fadeOutUp" isVisible={this.state.step1}>
                    <div className="step1">
                      <div className="row">
                        <div className="input-field col s12">
                          <input required placeholder="Name" id="name" name="name" type="text" className="validate" onChange={this.changeValue} />
                        </div>
                      </div>
                      <div className="row">
                        <div className="input-field col s12">
                          <input required placeholder="Address" id="address" name="address" type="text" className="validate" onChange={this.changeValue} />
                        </div>
                      </div>
                      <div className="row">
                        <div className="input-field col s12">
                          <input required placeholder="Contact Email" id="mail" name="mail" type="email" className="validate" onChange={this.changeValue} />
                        </div>
                      </div>
                    </div>
                  </Animated>
                  <Animated animationIn="fadeInUp" animationOut="fadeOut" isVisible={this.state.step2}>
                    <div className="step2">
                      <div className="row">
                        <div className="input-field col s12">
                          <input placeholder="Contact phone" id="phone" name="phone" type="text" className="validate" onChange={this.changeValue} />
                        </div>
                      </div>
                      <div className="row">
                        <div className="input-field col s12">
                          <select
                            required
                            id="time"
                            name="time"
                            value="morning"
                            onChange={this.changeValue}
                          >
                            <option value="morning">Morning</option>
                            <option value="afternoon">Afternoon</option>
                            <option value="night">Night</option>
                          </select>
                          <label htmlFor="time">At what time can we talk to you?</label>
                        </div>
                      </div>
                    </div>
                  </Animated>
                </div>
              }

              {this.state.processing ? <Button disable text="Processing..." /> : <Button text={this.state.buttonText} />}
            </form>
          }
          { this.props.successSend &&
            <h3>We are done!</h3>
          }
        </div>
      </div>
    );
  }
}

const mapStateProps = reduxState => ({
  passVerify: reduxState.panel.pass,
  successSend: reduxState.panel.send,
});

const mapDispatchToProps = {
  dispatchVerifyZipCode: panelActions.verifyZipCode,
  dispatchApprove: panelActions.approve,
  dispatchNotApply: panelActions.doesNotApply,
  dispatchSendInfo: panelActions.sendInfo,
};

export default connect(mapStateProps, mapDispatchToProps)(Form);
