import React from 'react';
import moment from 'moment';

import '../../../style/Footer.styl';
import world from '../../../img/worldpng.png';

export const Footer = () => (
  <footer className="section grey-text text-lighten-2 center-align ">
    <div className="overlayWorld" />
    <div className="worldContainer">
      <img src={world} alt="World" />
    </div>
    <h1>LET´S SAVE <span>OUR</span> WORLD!</h1>
    <span className="copy">Florida solar Power Copyright &copy; {moment().year()}</span>
  </footer>
);
