import React from 'react';
import PropTypes from 'prop-types';

export const H2 = ({ customClass, text }) => (
  <h2 className={customClass}>{ text }</h2>
);

H2.defaultProps = {
  customClass: '',
};
H2.propTypes = {
  customClass: PropTypes.string,
  text: PropTypes.string.isRequired,
};
