import React from 'react';
import { H2 } from './H2';

// Images from src
import employee from '../../../img/pic.jpg';
import frame from '../../../img/yellow_frame.png';
import eco from '../../../img/eco.png';
import embasy from '../../../img/embasy.png';
import saving from '../../../img/saving.png';

const tempText = 'Apply to receive a totally free visit from our technican for an inspection';

export const InfoSection = () => (
  <div className="row valign-wrapper">
    <div className="col s12 m6 l6 xl6">
      <H2 text="IT'S TIME FOR A CHANGE" />
      <table>
        <tbody>
          <tr>
            <td> <div className="Icon"> <img src={saving} alt="Icon Saving" /> </div> </td>
            <td><p><span>Save a lot of money</span> {tempText}</p></td>
          </tr>
          <tr>
            <td> <div className="Icon"> <img src={embasy} alt="Icon Embasy" /> </div> </td>
            <td><p><span>Receive financing</span> {tempText}</p></td>
          </tr>
          <tr>
            <td> <div className="Icon"> <img src={eco} alt="Icon Eco" /> </div> </td>
            <td><p><span>Make a positive impact on the planet</span> {tempText}</p></td>
          </tr>
        </tbody>
      </table>
    </div>
    <div className="col s12 m6 l6 xl6">
      <img className="responsive-img" src={employee} alt="Employee" />
      <img src={frame} alt="Shape" id="shape" />
    </div>
  </div>
);
