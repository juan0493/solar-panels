import React from 'react';
import { Parallax } from 'react-scroll-parallax';

import Image from './Image';
import '../../../style/HomeParallax.styl';

// Images
import cloudPhoto from '../../../img/cloud.jpg';
import panelsPhoto from '../../../img/PANELS.png';
import sunPhoto from '../../../img/sun.png';
import Logo from '../../../img/log.png';

export const HomeParallax = () => (
  <div className="containerParallax">
    <img src={panelsPhoto} alt="PANELS" id="panels-parallax" />
    <Parallax
      className="LogoSite valign-wrapper"
      offsetYMax={400}
      offsetYMin={-300}
      offsetXMax={0}
      offsetXMin={0}
    >
      <Image src={Logo} />
    </Parallax>
    <Parallax
      className="HomeParallax"
      offsetYMax={0}
      offsetYMin={0}
      offsetXMax="10%"
      offsetXMin="-25%"
      slowerScrollRate
    >
      <Image src={cloudPhoto} />
    </Parallax>
    <Parallax
      className="sun-parallax"
      offsetYMax={0}
      offsetYMin={-75}
      offsetXMax={0}
      offsetXMin={0}
      slowerScrollRate
    >
      <Image src={sunPhoto} />
    </Parallax>
  </div>
);
