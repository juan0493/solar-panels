import React from 'react';
import Form from './Form';

export const FormSection = () => (
  <div className="row">
    <div className="col s12">
      <Form />
    </div>
  </div>
);
