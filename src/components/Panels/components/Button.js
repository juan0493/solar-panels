import React from 'react';
import PropTypes from 'prop-types';
import '../../../style/Button.styl';


export const Button = ({ text, customClass, disable }) => (
  <div className="contButton">
    <button disabled={disable} className={`yellowButton ${customClass}`} type="submit">{text}</button>
  </div>
);

Button.defaultProps = {
  customClass: '',
  disable: false,
};
Button.propTypes = {
  customClass: PropTypes.string,
  text: PropTypes.string.isRequired,
  disable: PropTypes.bool,
};

