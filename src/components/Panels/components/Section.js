import React from 'react';
import PropTypes from 'prop-types';
import '../../../style/Sections.styl';

export const Section = props => (
  <section className={`valign-wrapper ${props.customClass}`}>
    {props.children}
  </section>
);


Section.defaultProps = {
  customClass: '',
};
Section.propTypes = {
  customClass: PropTypes.string,
  children: PropTypes.element.isRequired,
};
