import { panelTypes } from '../actionTypes';

const initialState = {
  pass: false,
  send: false,
};

export const panel = (state = initialState, action) => {
  switch (action.type) {
    case panelTypes.APPROVED:
      return {
        ...state,
        pass: true,
      };
    case panelTypes.NOT_APPLY:
      return {
        ...state,
        pass: false,
      };
    case panelTypes.SEND_SUCCESS:
      return {
        ...state,
        send: true,
      };
    case panelTypes.FAIL_SUCCESS:
      return {
        ...state,
        send: false,
      };
    default:
      return state;
  }
};
