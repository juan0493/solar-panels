import { combineReducers } from 'redux';
import { localeReducer as locale } from 'react-localize-redux';

import { panel } from './panel';

export const rootReducer = combineReducers({ panel, locale });
